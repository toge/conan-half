#include <iostream>
#include "half.hpp"

int main() {
    auto a = half_float::half{3.4};
    auto b = half_float::half{5};
    auto c = a * b;
    c += 3;
    if (c > a) {
        std::cout << c << std::endl;
    }
}
