from conans import ConanFile, tools

class HalfConan(ConanFile):
    name = "half"
    version = "2.1.0"
    license = "MIT"
    author = "toge toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-half"
    description = "<Description of Half here>"
    topics = ("half", "floating point", "header only")
    no_copy_source = True

    def source(self):
        tools.get("https://svwh.dl.sourceforge.net/project/half/half/{0}/half-{0}.zip".format(self.version))

    def package(self):
        self.copy("*.hpp", "include", keep_path=False)
